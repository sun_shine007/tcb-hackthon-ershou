//app.js
App({
    onLaunch: function() {

        if (!wx.cloud) {
            console.error('请使用 2.2.3 或以上的基础库以使用云能力')
        } else {
            wx.cloud.init({
                // env 参数说明：
                //   env 参数决定接下来小程序发起的云开发调用（wx.cloud.xxx）会默认请求到哪个云环境的资源
                //   此处请填入环境 ID, 环境 ID 可打开云控制台查看
                //   如不填则使用默认环境（第一个创建的环境）
                env: '环境id',
                traceUser: true,
            })
        }

        this.globalData = {}
        if (wx.getStorageSync('openid') != '') {
            this.globalData.openid = wx.getStorageSync('openid')
        }else{
            wx.cloud.callFunction({
                name: 'login',
                data: {},
                success: res => {
                    console.log('登录函数',res.result)
                    wx.setStorageSync('openid', res.result.openid)
                    this.globalData.openid = res.result.openid
                },
                fail: err => {
                    wx.showToast({
                        icon: 'none',
                        title: '请检查登陆状态',
                    })
                    console.log('[云函数] [login] 获取 openid 失败，请检查是否有部署云函数，错误信息：', err)
                }
            })
        }
        
        console.log(this.globalData.openid)
    }
})